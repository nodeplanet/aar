var LocalStrategy= require('passport-local').Strategy;
var User= require('../model/Student');
var bCrypt= require('bcrypt-nodejs');

module.exports= function(passport){
  passport.use('studentLogin',
        new LocalStrategy({ usernameField: 'studentUsername',
                            passwordField: 'studentPassword',
                            passReqToCallback: true
                          },
        function(req,username,password,done){
          User.findOne({username: username},function(err,user){
                    if(err){
                            console.log("Error while finding user");
                            return done(err);
                    }
                    if(!user){
                            console.log("user not found");
                            return done(null, false, req.flash('message', 'User Not found.'));
                    }
                    console.log("hi"+user);
                    if(!isValidPassword(user,password)){
                      console.log("Invalid Password");
                      return done(null,false, req.flash('message', 'Invalid Password'));
                    }

                    return done(null,user);
          });
        })

);
    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }
}
