/* Neeraj Code

var signup = require('./signup-student');
var User = require('../model/Student');
Neeraj code
*/
var hod_user = require('../model/hodDb.js');
var mentor_user = require('../model/mentordb.js');
var student_user= require('../model/Student.js');
var hodlogin = require('./hodlogin');
var studentLogin = require('./studentLogin');
var mentorlogin = require('./mentorlogin');
module.exports = function(passport){

	// Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        done(null, JSON.stringify({id : user._id, desig : user.designation}));
    });

    passport.deserializeUser(function(json, done) {

        var theUser = JSON.parse(json);
        if(theUser.desig == "mentor"){
            mentor_user.findById(theUser.id, function(err, user) {
                //console.log('deserializing user:',user);
                done(err, user);
            });
        }
        else if(theUser.desig == "hod"){
            hod_user.findById(theUser.id, function(err, user) {
                //console.log('deserializing user:',user);
                done(err, user);
            });
        }
        else if(theUser.desig== "student"){
            student_user.findById(theUser.id,function(err,user){
              done(err,user);
            });

        }
    });

    // Setting up Passport Strategies for Login and SignUp/Registration

    hodlogin(passport);
    studentLogin(passport);
    mentorlogin(passport);
}
