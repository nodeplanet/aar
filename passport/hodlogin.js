var LocalStrategy   = require('passport-local').Strategy;
var User = require('../model/hodDb');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(passport){

	passport.use('loginhod',
        new LocalStrategy({ usernameField: 'hodUsername',
                            passwordField: 'hodPassword',
                            passReqToCallback : true
                             },
        function(req, username, password, done) {
            // check in mongo if a user with username exists or not
            User.findOne({ 'username' :  username },
                function(err, user) {
                    // In case of any error, return using the done method
                    if (err)
                        return done(err);
                    // Username does not exist, log the error and redirect back
                    if (!user){
                        console.log('User Not Found with username '+username);
                        return done(null, false, req.flash('message', 'User Not found.'));
                    }
                    console.log('hi--'+user);
                    // User exists but wrong password, log the error
                    if (!isValidPassword(user, password)){
                        console.log('Invalid Password');
                        return done(null, false, req.flash('message', 'Invalid Password')); // redirect back to login page
                    }
                    // User and password both match, return user from done method
                    // which will be treated like success
                    //                bi747console.log('hi--'+user);
                    // console.log('hihiihi'+password);
                    return done(null, user);
                }
            );

        })
    );


    var isValidPassword = function(user, password){
        return bCrypt.compareSync(password, user.password);
    }

}
/*
sub.json >>

cse:{
    1:[cs,cd,,vyh,yy,ht],
    2:{[]},
    },

mech:{
    1:{[....]};
    2:{[.....]};
    },
    .
    .
    .
    .


*/
