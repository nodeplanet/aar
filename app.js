var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');



var users = require('./routes/users');
var hodsignup = require('./routes/hodsignup');

/* rohan

var mongo = require('mongodb');
  var monk = require('monk');
  var db = monk('localhost:27017/notesharing');
  */

/*var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/notesharing');
var db = mongoose.connection;*/
var passport=require('passport');
var expressSession = require('express-session');
var flash = require('connect-flash');

var initPassport = require('./passport/init');  //initializing passport
var routes = require('./routes/index')(passport);

/*
var users = require('./routes/users');
var login=require('./routes/login')
var mongo=require('./routes/mongo-connection');
*/
var mongoose = require('mongoose');
// Connect to DB
mongoose.connect("mongodb://localhost:27017/aardb");








var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'uploads')));

/* rohan
app.use(function(req,res,next){
  req.db=db;
  next();
}); */
app.use(expressSession({secret: 'mySecretKey'}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

initPassport(passport);

app.use('/', routes);
app.use('/users', users);
app.use('/hodSignupPost',hodsignup);

var routes = require('./routes/index')(passport);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
