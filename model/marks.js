var mongoose = require('mongoose')

module.exports = mongoose.model('marks',{
                rollno : Number,
                batch : Number,
                subjectPattern: Number,
                groupName : String,
                mentorUsername : String,
                hod_id : String,
                marks: mongoose.Schema.Types.Mixed,
                DMCpics: mongoose.Schema.Types.Mixed,
                sessional : mongoose.Schema.Types.Mixed
});
