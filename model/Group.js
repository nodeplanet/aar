var mongoose= require('mongoose');

module.exports=mongoose.model('Group',{
  batch: Number,
  department: String,
  groupName: String,
  startingRollNo: Number,
  endingRollNo: Number,
  mentorUsername: String
});
