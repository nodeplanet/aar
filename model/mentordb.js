var mongoose = require('mongoose')

module.exports = mongoose.model('mentordb',{
	name: String,
	mentorUsername: String,
	department:String,
	password: String,
	groupName: String,
	batch: Number,
	designation: String
});
