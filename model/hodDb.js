var mongoose = require('mongoose')

module.exports = mongoose.model('hoddb',{
	name: String,
	username: String,
	department:String,
	password: String,
	designation: String
});
