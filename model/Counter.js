var mongoose= require('mongoose');

module.exports=mongoose.model('Counter',{
  name: String,
  seq: {type: Number, default: 0}
});
