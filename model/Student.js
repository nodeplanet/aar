var mongoose=require('mongoose');

module.exports=mongoose.model('Student',{
  firstname: String,
  lastname: String,
  fathername: String,
  address: String,
  rollno: Number,
  batch: Number,
  department: String,
  email: String,
  phoneNo: Number,
  groupName: String,
  username: String,
  password: String,
  designation: String,
  mentorUsername: String
});
