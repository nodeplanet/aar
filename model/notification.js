var mongoose = require('mongoose');

module.exports = mongoose.model('notification',{
                notificationId: Number,
                rollno : String,
                name: String,
                batch : Number,
                groupName : String,
                mentorUsername : String,
                requestedMarks: mongoose.Schema.Types.Mixed,
                verified: String,
                dmcName: String,
                status: Number,
                semester: Number,
                DMCpic: String

});
