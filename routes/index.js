var express = require('express');
var multer= require('multer');
var router = express.Router();
var student= require('../model/Student.js');
var mentorDb= require('../model/mentordb.js');
var hodDb = require('../model/hodDb.js');
var group = require('../model/Group.js');
var marks= require('../model/marks.js');
var bCrypt = require('bcrypt-nodejs');
var notification= require('../model/notification.js');
var counter= require('../model/Counter.js');

///////////////////////reading subjects from json file\\\\\\\\\\\\\\\\\\\\\\\\\\\\
var fs=require('fs');
var subjectObject;
var subjectPatternArray;
var DMCpatternArray=[null,null,null,null,null,null,null,null];
fs.readFile('subjects.json','utf8',function(err ,data){
  if(err){
    console.log(err);
  }else{
  subjectObject=JSON.parse(data);
  subjectPatternArray=Object.keys(subjectObject);
  //console.log(subject_test);
  }
});


/*var sem_subConstructor = function(){
  return [
  {"sub1": null,"sub2": null,"sub3": null,"sub4": null,"sub5": null,"sub6":null},
  {"sub7":null,"sub8":null,"sub9":null,"sub10":null,"sub11":null,"sub12":null},
  {"sub13":null,"sub14":null,"sub15":null,"sub16":null,"sub17":null,"sub18":null},
  {"sub19":null,"sub20":null,"sub21":null,"sub22":null,"sub23":null,"sub24":null},
  {"sub25":null,"sub26":null,"sub27":null,"sub28":null,"sub29":null},
  {"sub30":null,"sub31":null,"sub32":null,"sub33":null,"sub34":null},
  {"sub35":null,"sub36":null,"sub37":null,"sub38":null,"sub39":null},
  {"sub40":null,"sub41":null,"sub42":null,"sub43":null }
]};
*/

var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
	// Passport adds this method to request object. A middleware is allowed to add properties to
	// request and response objects
	if (req.isAuthenticated()){
		return next();
	}
	// if the user is not authenticated then redirect him to the login page
		res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
		res.render('loginHod', { message: req.flash('message'), user: "login", auth:false,title:"login"});
}

var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

/* GET home page. */
router.get('/',function(req, res, next) {
    //console.log(subject);
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('index', { title: 'AAR-home', 'user' : req.user, message: req.flash('message')});
/*
    //////////////////// for sessional demo -- to be ported later --//////////////////
    var i=0;
    var sess = [],jsoo = new Object,sem = {};
    var marksInstance= new marks();
    var jdata = subjectObject[2013];
    for(i;i<8;i++){
    console.log("hii -->");
    var kee = Object.keys(jdata[i]);
        for(x of kee){
         jsoo[x] = null;
        }
        sess.push(jsoo);
        sess.push(jsoo);
        sess.push(jsoo);
        sem[i] = sess;
        sess = [];
        jsoo = {};

    }
    console.log(sem);

*/

});

router.get('/marksView',function(req,res,next){
  if(req.isAuthenticated() && req.user.designation == 'student'){
    console.log(req.user.rollno);
    //marks database fetched and rendered
    marks.findOne({rollno: req.user.rollno},function(e,docs){
      console.log(docs.marks);
      res.render('marksView',{title : "Record",record: docs, marks: JSON.stringify(docs.marks),auth : true })
    })

  }
  else{
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render("loginStudent",{title: 'Login - Student', message: req.flash('message'),auth: false})
  }
});

var multerDmcUploadObject;                  // It holds the multer object settings

var storageDMC=multer.diskStorage({
      destination: function(req,file,cb){
          cb(null,__dirname+'/../uploads/DMC');
      },
      filename: function(req,file,cb){
        var ext= file.originalname.split(".");
        cb(null, 'a'+Date.now()+'.'+ext[1]);
      }
});

var fileFilterDMC= function(req,file,cb){
  var type=file.mimetype.split('/');
  if(type[0]== 'image')
    cb(null,true);
  else {
    console.log("Wrong File Type Choice")
    cb(null,false);
  }
}

multerDmcUploadObject=multer({storage: storageDMC,fileFilter: fileFilterDMC});    //initializing multer upload object with the settings


router.post('/requestMarksPost',multerDmcUploadObject.single("uploadDMC"),function(req,res){
  console.log(req.body.requestField);
  var requestField= req.body.requestField;
  console.log(req.body.rollno);
  student.findOne({rollno: req.body.rollno},function(err,user){
        if(err){
          console.log("error in finding the student");

        }else{
                  var notificationInstance= new notification();
                  counter.update({name: "notificationId"},{$inc: { seq: 1}},{upsert: true},function(err,doc){
                  counter.find({"name" : "notificationId"},function(err,doc){
                  console.log(doc);
                  notificationInstance.notificationId=doc[0].seq;
                  console.log(notificationInstance.notificationId);
                  notificationInstance.rollno= req.body.rollno;
                  notificationInstance.batch= user.batch;
                  notificationInstance.groupName= user.groupName;
                  notificationInstance.mentorUsername= user.mentorUsername;
                  notificationInstance.verified= "No";
                  notificationInstance.status= null;
                  console.log(req.body.semester);
                  console.log("hh");
                  notificationInstance.semester= req.body.semester;
                  notificationInstance.requestedMarks= JSON.parse(req.body.requestField);
                  notificationInstance.name=user.firstname;
                  notificationInstance.DMCpic=req.file.filename;
                  console.log("Hai");

                  notificationInstance.save(function(err){
                    if(err){
                      console.log(err);
                    }
                    else{
                      console.log("notification instance saved successfully");
                      console.log(notificationInstance)
                      res.send('Sent for verification')
                    }


                  });

                });


              });

              //val.toNumber();

        }
  });
});



router.get('/homehod',function(req,res,next){
    if(req.isAuthenticated()){
        if(req.user.designation == 'hod'){
            res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            res.render('homehod',{title:'Home-HOD','user' : req.user,message: req.flash('message')});
        }else
            res.send('Unautharised user');
    }
  else {
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('loginHod',{title:'Login - HOD',message:
         req.flash('message')});
      }
});

router.get('/homeMentor',function(req,res,next){
    if(req.isAuthenticated()){
        if(req.user.designation == 'mentor'){
            console.log(req.user.mentorUsername);
            notification.find({mentorUsername: req.user.mentorUsername},function(err,doc){
              if(err){
                console.log("Error in finding notification");

              }else{
                console.log(doc);
                res.render('homeMentor',{title:'Home-Mentor','user' : req.user,'notification' : JSON.stringify(doc), message: req.flash('message')});

              }

            });

        }else
            res.send('Unautharised user');
    }
  else{
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
        res.render('loginMentor',{title:'Login - Mentor',message: req.flash('message')});
}
});

router.get('/showDMC',function(req,res){
  console.log(req.query.name);
  res.render('showDMC',{title: "DMC",rollno: req.query.rollno,picName: req.query.picName});
});
router.get('/sessionalView',function(req,res,next){
  if(req.isAuthenticated()){
        if(req.user.designation == 'mentor'){
            console.log(req.user.mentorUsername);
            res.render('sessionalView',{title:'AAR-Sessional','user' : req.user, message: req.flash('message')});
        }else
            res.send('Unautharised user');
    }
  else
        res.render('loginMentor',{title:'AAR-Mentor Login',message: req.flash('message')});
})

router.get('/sessionalEdit',function(req,res,next){
  if(req.isAuthenticated()){
        if(req.user.designation == 'mentor'){
            console.log(req.user.mentorUsername);

            res.render('sessionalEdit',{title:'AAR-Sessional','user' : req.user, message: req.flash('message')});
        }else
            res.send('Unautharised user');
    }
  else
        res.render('loginMentor',{title:'AAR-Mentor Login',message: req.flash('message')});
});


router.post('/verifyMarksPost',function(req,res){
  console.log(req.body.replyServerField);
  var serverField=JSON.parse(req.body.replyServerField);
  var notificationIdRec=Number(serverField.notificationId);
  var statusRec=Number(serverField.status);
  var semester=Number(serverField.semester);
  console.log(semester);
  if(statusRec==0){
    /*
      notification.update({notificationId: notificationIdRec },{$set: {status: statusRec}},function(err,doc){
      if(err){
          console.log("Error");
      }
      else{
          if(doc==1){
                notification.remove({notificationId: notificationIdRec},function(err,doc){
                if(err){
                console.log(err);
                }
                else{
                console.log(doc);
                }

                });
          }
        }


      }); */
      notification.remove({notificationId: notificationIdRec},function(err,doc){
      if(err){
      console.log(err);
      }
      else{
      console.log(doc);
      res.redirect('/homementor');
      }

      });
}

else if(statusRec==1){
  console.log("Blab");
  notification.findOne({notificationId: notificationIdRec},function(err,doc){
        if(err){
            console.log("Error"+err);
        }
        else{
            console.log(doc);
            marks.findOne({rollno: doc.rollno},function(err,user){
              if(err){
                console.log(err);
              }
              else{
                console.log(user);
                var update=user.marks[semester-1];
                console.log(update);
                for (var x in doc.requestedMarks){
                  for (var y in update){
                    if(x==y){
                      console.log(y);
                      update[y]=doc.requestedMarks[y];

                    }
                  }

                }
                console.log(update);
                user.marks[semester-1]=update;
                //user.DMCpics[semester-1]=doc.DMCpic;
                console.log(user.marks);
                console.log(user);
                marks.update({rollno: doc.rollno},{$set:{marks: user.marks,DMCpics: user.DMCpics}},function(err,doca){
                    if(err){
                          console.log("err");
                    }
                  else{
                    console.log("marks updated="+ doca);
                    notification.remove({notificationId: notificationIdRec},function(err,doc){
                    if(err){
                    console.log(err);
                    }
                    else{
                    //console.log(doc);
                    console.log(notificationIdRec+"removed");
                    }
                    });

                    }

                    });

                res.send("Accepted");

                }


                });
              }



        });


  }
});

router.get('/loginhod',function(req,res,next){
  if(req.isAuthenticated()){
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('homehod',{title:'Home-HOD','user' : req.user,message: req.flash('message')});
  }else{
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	res.render('loginHod',{title:'Login - HOD', message: req.flash('message')});
}
});

router.get('/loginmentor',function(req,res,next){
  if(req.isAuthenticated()){
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('index', { title: 'AAR-home', 'user' : req.user, message: req.flash('message')});
  }else{
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	res.render('loginMentor',{title:'Login - Mentor', message: req.flash('message')});
}
});

router.get('/creatementor',function(req,res,next){
  if(req.isAuthenticated()){
        if(req.user.designation == 'hod')
            res.render('createMentor',{title:'Signup - Mentor'});
        else
            res.send('Aunautharised user');
    }
  else
        res.render('loginHod',{title:'Login - HOD',message: req.flash('message')});

});

router.get('/createhod',function(req,res,next){
  if(1)
	   res.render('createHod',{title:'Signup - HOD'});

});


router.post('/createhodpost',function(req,res,next){

	var hod = new hodDb();

	hod.name = req.body.hodName;
	hod.username = req.body.hodUsername;
	hod.password = createHash(req.body.hodPassword);
	hod.department = req.body.department;
	hod.designation= "hod";
  hodDb.findOne({ username:hod.username },function(err,user){
                          if(err){
                            console.log('Error in SignUp'+err);
                            return done(err);

                          }
                          if(user){
                            console.log('User already exists');
                            res.send('User already exists');

                          }else{
	hod.save(function(err) {
                            if (err){
                                console.log('Error in Saving hod: '+err);
                                throw err;
                            }
                            else
                            	res.send('HOD added to database');
                            	console.log(hod);
                        	});
}
});
	//res.render('hod-signup',{title:'Signup - HOD'});
});

router.post('/creatementorpost',function(req,res,next){
	var mentor = new mentorDb();

	mentor.name = req.body.mentorName;
	mentor.mentorUsername = req.body.mentorUsername;
	mentor.password = createHash(req.body.mentorPassword);
	mentor.department = req.body.department;
	mentor.designation = "mentor";
  mentorDb.findOne({ mentorUsername:mentor.mentorUsername },function(err,user){
                          if(err){
                            console.log('Error in SignUp'+err);
                            return done(err);

                          }
                          if(user){
                            console.log('User already exists');
                            res.send('User already exists');

                          }else{
	mentor.save(function(err) {
                            if (err){
                                console.log('Error in Saving mentor: '+err);
                                throw err;
                            }
                            else
                            	res.send('added to Mentor db');
                            	console.log(mentor);
                        	});
}
});
});

module.exports = router;



/////////////////  PASSPORT ///////////////////

module.exports = function(passport){

router.get('/studentSignup', function(req,res){
  if(req.isAuthenticated()){
      if(req.user.designation=='hod')
        res.render('studentSignup',{title: 'studentSignup',message: req.flash('message')});
      else
        res.send('Unautharised access');
  }
  else
    res.render('loginHod',{title:'Login - HOD',message: req.flash('message')});
});
/*
router.post('/studentSignupPost', passport.authenticate('signup-student',{
  successRedirect: '/',
	failureRedirect: '/studentSignup',
	failureFlash : true

})); */
router.post('/studentSignupPost',function(req,res){
      var studentInstance={}
      studentInstance.firstname=req.body.studentFirstname;
      studentInstance.lastname=req.body.studentLastname;
      studentInstance.fathername=req.body.studentFathername;
      studentInstance.address=req.body.studentAddress;
      studentInstance.rollno=req.body.studentRollno;
      studentInstance.department=req.body.studentDepartment;
      studentInstance.email=req.body.studentEmail;
      studentInstance.phoneNo=req.body.studentPhoneno;
      studentInstance.username=req.body.studentUsername;
      studentInstance.password=createHash(req.body.studentPassword);
			studentInstance.designation="student";

      student.update({rollno: studentInstance.rollno},{$set: studentInstance},function(err,docs){
        if(err){
          console.log("err");

        }else{
          console.log("updated successfully");

        }

      });
      res.send('Student Created');
});


router.post('/checkStudentSignup',function(req,res){
  console.log("hai");
  var data={
        usernameStatus: 1,
        emailStatus: 1
  };
  student.findOne({username: req.body.username},function(err,user){
    if(user){
          console.log(user);
          data.usernameStatus=0;
          //res.status(200).json(data);

    }
    student.findOne({email : req.body.email},function(err,user){
      if(user){
            console.log(user);
            data.emailStatus=0;
      }
      console.log(data);
      res.status(200).json(data);

    });
  });

});

router.get('/loginStudent',function(req,res){
  if(req.isAuthenticated()){
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.render('index', { title: 'AAR-home', 'user' : req.user, message: req.flash('message')});
  }
  else{
  res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
	res.render("loginStudent",{title: 'student login', message: req.flash('message'),auth: false})
  }
});

router.post('/studentLoginPost',passport.authenticate('studentLogin',{
	successRedirect: '/marksview',
	failureRedirect: '/loginStudent',
	failureFlash : true
}));
router.post('/hodloginpost',passport.authenticate('loginhod', {
  successRedirect: '/homehod',
  failureRedirect: '/loginhod',
  failureFlash : true
}));

router.post('/mentorloginpost',passport.authenticate('loginmentor', {
		successRedirect: '/homeMentor',
		failureRedirect: '/loginmentor',
		failureFlash : true
	}));

router.get('/signout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/createGroup',function(req,res){
  if(req.isAuthenticated()){
      if(req.user.designation == "hod")
        res.render('createGroup',{title: "Create Group", message: req.flash('message'),user: req.user,auth: true});
      else
        res.send('Unautharised access');
  }
  else
    res.render('loginHod',{title:'Login - HOD',message: req.flash('message')});
});

router.post('/groupPost',function(req,res){
  var groupInstance= new group();
  var rollno;
  groupInstance.batch= req.body.batch;
  groupInstance.department= req.body.department;
  groupInstance.startingRollNo= req.body.startingRollNo;
  groupInstance.endingRollNo= req.body.endingRollNo;
	groupInstance.groupName= req.body.groupName;
  console.log(groupInstance);
  groupInstance.save(function(err){
    if(err){
      console.log("Error in saving groupInstance");
    }
    else{
        student.update({$and: [{ rollno: {$gte: req.body.startingRollNo}}, { rollno: {$lte: req.body.endingRollNo}}]},{$set: {groupName: groupInstance.groupName}},{multi: true},function(err,docs){
          if(err){
            console.log("error in updating"+err);
          }
          else{
            console.log(docs);
						console.log(req.body.startingRollNo);
						marks.update({$and: [{ rollno: {$gte: req.body.startingRollNo}}, { rollno: {$lte: req.body.endingRollNo}}]},{$set: {groupName: req.body.groupName}}, {multi: true}, function(err,docs){
														if(err){
															console.log(err);
														}
														else{
															console.log(docs);
														}
						});

					}

        });

    }

  });
  res.send('Group Created Successfully');
});

router.get('/createBatch',function(req,res){
  if(req.isAuthenticated()){
      if(req.user.designation=='hod')
          //console.log(subjectPatternArray);
          res.render('createBatch',{title: 'createBatch',user: req.user, message: req.flash('message'), subjectPattern: subjectPatternArray, auth: true});
      else
          res.send('Unautharised access')
  }
  else
      res.render('loginHod',{title:'Login - HOD',message: req.flash('message')});
});

router.post('/batchPost',function(req,res){
  var rollno;
  console.log(req.body.selectedSubjectPattern);
  for(rollno=req.body.startingRollNo;rollno<=req.body.endingRollNo; rollno++){
            var studentInstance= new student();
            studentInstance.rollno= rollno;
            studentInstance.batch= req.body.batch;
            studentInstance.department= req.body.department;
            studentInstance.save(function(err){
                  if(err){
                    console.log("error");
                  }
                  else{
                    console.log("created successfully");


                  }
            });
						var marksInstance= new marks();
						marksInstance.rollno= studentInstance.rollno;
						marksInstance.batch= req.body.batch;
            marksInstance.subjectPattern=req.body.selectedSubjectPattern;
            //////////////////////////////////////////////////////// subject_object called
            console.log(subjectObject[req.body.selectedSubjectPattern]);
            marksInstance.marks=subjectObject[req.body.selectedSubjectPattern];
            marksInstance.DMCpics=DMCpatternArray;
            console.log("Hii"+marksInstance.DMCpics);
//////////////// for sessional -- starts ////////////////
              var i=0;
              var sess = [],jsoo = new Object,sem = {};
              var jdata = subjectObject[req.body.selectedSubjectPattern];
              for(i;i<8;i++){

              console.log("hii -->");
              var kee = Object.keys(jdata[i]);
                  for(x of kee){
                   jsoo[x] = null;
                  }
                  sess.push(jsoo);
                  sess.push(jsoo);
                  sess.push(jsoo);
                  sem[i] = sess;
                  sess = [];
                  jsoo = {};

              }
              console.log(sem);
              marksInstance.sessional = sem;
/////////////// for sessional --  ends ///////////////
					  marksInstance.save(function(err){
							if(err){
								console.log("error while saving marksInstance");
							}
							else{
								console.log("marksInstance saved successfully");
							}
						});
   }
  res.send('Batch Created');
});

router.get('/assignMentor',function(req,res){
  if(req.isAuthenticated()){ 
      if(req.user.designation=='hod'){

      mentorDb.find({},{name: 1, mentorUsername: 1},function(err,docs){
        if(err){
          console.log(err);
        }else{
          console.log(docs);
          group.find({},{batch: 1,groupName: 1},function(err,groupDocs){
              if(err){
                console.log(err);
              }else{
                console.log(groupDocs);
                res.render("assignMentor",{title: 'Assign Mentor-group',user: req.user,mentorArray: docs,groupArray: groupDocs,message: req.flash('message'),auth: true});
              }
          });
        }
      });
  }
}
else
  //res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  res.render('loginHod',{title:'Login - HOD',message: req.flash('message')});
});


router.post("/assignMentorPost",function(req,res){
  var mentorSelected= req.body.mentorSelected;
  var groupSelected= JSON.parse(req.body.groupSelected);
  console.log(mentorSelected);
  console.log(groupSelected);
  mentorDb.update({mentorUsername: req.body.mentorSelected},{$set: {batch: groupSelected.batch, groupName: groupSelected.groupName}},function(err,docs){
    if(err){
      console.log(err);
    }
    else{
      console.log("Mentor Updated"+docs);
    }
  });
  group.update({batch: groupSelected.batch,groupName: groupSelected.groupName},{$set:{mentorUsername: mentorSelected}},function(err,docs){
    if(err){
      console.log(err);

    }
    else{
      console.log("group updated"+docs);
    }


  });
group.findOne({batch: groupSelected.batch,groupName: groupSelected.groupName},{startingRollNo: 1,endingRollNo: 1},function(err,docs){
  if(err){
    console.log(err);
  }
  else{
  console.log(docs);
  student.update({$and: [{rollno: {$gte: docs.startingRollNo}},{rollno: {$lte: docs.endingRollNo}}]},{$set:{mentorUsername: mentorSelected}},{multi: true},function(err,doc){
    if(err){
      console.log(err);
    }
    else{
      console.log("student updated"+doc);
    }

  });
  marks.update({$and: [{rollno: {$gte: docs.startingRollNo}},{rollno: {$lte: docs.endingRollNo}}]},{$set:{mentorUsername: mentorSelected}},{multi: true},function(err,docss){
    if(err){
      console.log(err);
    }
    else{
      console.log("student also updated in marks database"+docss);
    }

  });

}
});
res.send("Mentor Assigned");





});

return router;
}
